/*
 * Сейчас умею:
 * - получать время по ntp, преобразовывать его внутри loop и выводить на LED
 * - запрос к NTP сделан через функцию с millis(), частота опроса NTP настраивается в define, между запросами время отсчитывается локально
 * - обновляю вермя целиком на LED, а не отдельно символы или точки. @ И так быстро, лень заморачиватся.
 * - выведен в define выбор моргать двоеточием или нет.
 * - добавлено чтение данных в MQTT и вывод на LED бегущей строкой
 * - добавлена коорекция в локальный расчет времени в секунды из NTP, что бы максимально близко соответсвовать
 * 
 * @добавить в Serial вывод времени звездочками имитриуя вывод LED матрицей, или лень.
 * добавить вывод звука
 *  
 */


//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#include "MaxMatrix.h"      // Библиотека для LED модулей
#include "main.h"           // переменные и дефайны
#include "pass.h"           // адреса, пароли и пр. персональные данные, что не нужно отдавать в git
#include <PubSubClient.h>   // комплектная из репозитория PlatformIO, для MQTT
#include <ESP8266WiFi.h>    // комплектная из репозитория PlatformIO, для сетевого взаимодействия

#ifdef NTP_ON
  #include <WiFiUdp.h>        // UDP необходим для запроса к NTP
#endif

//#########################################################
//###                  initialization                   ###
//#########################################################
#ifdef LED_CONNECTED
  MaxMatrix m(DIN, CS, CLK, MATRIX_MODULES);    // инициируем пины управления и количество модулей в матрице
#endif

WiFiClient espClient;             // для инициализации WiFi
PubSubClient client(espClient);   // для инициализации mqtt

#ifdef NTP_ON
  IPAddress timeServerIP;   // time.nist.gov NTP server address
  WiFiUDP udp;              // A UDP instance to let us send and receive packets over UDP
#endif



//#########################################################
//###                       SETUP                       ###
//#########################################################
void setup() {
  #ifdef SERIAL_DEBUG
    Serial.begin(115200);
    while(!Serial){};
    Serial.println();
    Serial.println("SERIAL DEBUG START HERE");
  #endif

  // если модули не подключены, но инициализируются, то esp уходит в перезагрузку по WDT
  #ifdef LED_CONNECTED
    m.init();                     // MAX7219 initialization
    m.setIntensity(LED_BRIGHT);   // initial led matrix intensity, 0-15
  #endif

  #ifdef SERIAL_DEBUG
    Serial.println("=============");
    Serial.print("HOUR = ");
    Serial.println(hours);
  
    Serial.print("MIN =  ");
    Serial.println(minutes);
  #endif

  printHours();
  printMinutes();
  m.writeSprite(15, 0, colon1); // рисуем двоеточие на LED

  setup_wifi();
  client.setServer(mqtt_server, MQTT_PORT); // указание IP/домена сервера MQTT и порта, пока не подключимся не выйдем из цикла внутри функции и не двинемся далее
  client.setCallback(callback);             // подписка на входящие сообщения в топиках, что будем мониторить, @ вызывать отдельно не надо, работает внутри client.loop()
}



//#########################################################
//###                       LOOP                        ###
//#########################################################
void loop() {
  
  if (!client.connected()) reconnect();   // постоянная проверка подключены ли мы к MQTT, естественно проверка включает проверку подключения по WiFi

  client.loop();        // некий loop библиотеки MQTT, внутри в частности опрашиваются топики на которые подписаны

  now = millis();       // Возвращает количество миллисекунд с момента начала выполнения текущей программы. сбрасывается в 0 по переполнению(max=4294967296) приблизительно через 50 дней(49 days and 17 hours)
  
  sendMQTT();           // постим сообщение в MQTT

  updateNTP();          // раз в NTP_TIMEOUT (поставил час) обновляем время по NTP
  firstBOOT = false;    // снимаем флаг первого запуска, должно быть после updateNTP(), т.к. писалось под него

  updateLocalTime();    // обновление минут локально по счетчику, с поправкой от NTP

  updateLEDtime();      // отрисовка обновленного времени

  dotsBlinking();       // моргаем двоеточием

} // END loop