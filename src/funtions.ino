//#pragma once
#include "main.h"           // переменные и дефайны

//#########################################################
//###                    Функции                        ###
//#########################################################

// функция печатает число на мтарице, на вход печатаемое число и координата Х
void printLED(uint8_t x, uint8_t value) {
    switch (value) {
      case 0:
        m.writeSprite(x, 0, zero1);
        break;
      
      case 1:
        m.writeSprite(x, 0, one);
        break;
      
      case 2:
        m.writeSprite(x, 0, two);
        break;

      case 3:
        m.writeSprite(x, 0, three1);
        break;

      case 4:
        m.writeSprite(x, 0, four1);
        break;

      case 5:
        m.writeSprite(x, 0, five1);
        break;
    
      case 6:
        m.writeSprite(x, 0, six1);
          break;

      case 7:
        m.writeSprite(x, 0, seven);
          break;

      case 8:
        m.writeSprite(x, 0, eigth);
          break;

      case 9:
        m.writeSprite(x, 0, nine);
          break;

      case 10:
        m.writeSprite(x, 0, nothing);
          break;
    }

}

// функция переводит значение одного вомзожно двузначного числа отдельно в единицы и десятки
void time2digits(uint8_t value) { 
    //Получаем десятки от value с помощью целочисленного деления
    timeDisp[0] = value / 10;
    //Получаем единицы от value с помощью остатка от деления
    timeDisp[1] = value % 10;
}

// инициируем вывод часов на матрицу
void printHours() {
    // @ тут можно ввести функцию, что будет обновлять только изменившиеся точки на LED, сравнивая со старым значением
    time2digits(hours);
    #ifdef PRINT_ZERO_HOURS 
      printLED(2, timeDisp[0]);
    #else
      if (  (timeDisp[0]) ) printLED(2, timeDisp[0]);
      if ( !(timeDisp[0]) ) printLED(2, 10);
    #endif
    
    printLED(8, timeDisp[1]);

    #ifdef SERIAL_DEBUG
      Serial.println("LED time output:");
      Serial.print(timeDisp[0]);
      Serial.print(timeDisp[1]);
    #endif
}

// инициируем вывод минут на матрицу
void printMinutes() {
    // @ тут можно ввести функцию, что будет обновлять только изменившиеся точки на LED, сравнивая со старым значением
    time2digits(minutes);
    printLED(19, timeDisp[0]);
    printLED(25, timeDisp[1]);

    #ifdef SERIAL_DEBUG
      Serial.print(" : ");
      Serial.print(timeDisp[0]);
      Serial.println(timeDisp[1]);
    #endif
}

// Put extracted character on Display
void printCharWithShift(char c, uint16_t shift_speed){
  if (c < 32) return;
  c -= 32;
  memcpy_P(buffer, CH + 7*c, 7);
  m.writeSprite(MATRIX_MODULES*8, 0, buffer);
  m.setColumn(MATRIX_MODULES*8 + buffer[0], 0);
  
  for (int i = 0; i < buffer[0]+1; i++) 
  {
    delay(shift_speed);
    m.shiftLeft(false, false);
  }
}

// Extract characters from Scrolling text
void printStringWithShift(char* s, uint16_t shift_speed){
  while (*s != 0){
    printCharWithShift(*s, shift_speed);
    s++;
  }
}

// установка WIFI соединения
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  #endif

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    #ifdef SERIAL_DEBUG
      Serial.print(".");
    #endif
  }

  udp.begin(localPort);
  //udp.localPort();

  #ifdef SERIAL_DEBUG
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address:   ");
    Serial.println(WiFi.localIP());
    
    Serial.println("Starting UDP");
    Serial.print("Local port:  ");
    Serial.println(udp.localPort());
  #endif
}

// функция чтения данных из топика MQTT
void callback(char* topic, byte* payload, uint16_t length) {  // наверняка хватило бы и uint8_t
  char ledString[length];             // буфер, объявленный локально для вывода строки в LED; @ было [length+1] хз зачем, может для добавления в конце "\0"

  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");
    Serial.print("Message:");
  #endif
  
  // функция для конвертации сообщения из MQTT
  for (i = 0; i < length; i++) {
    #ifdef SERIAL_DEBUG
      Serial.print((char)payload[i]);
    #endif
    ledString[i] = (char)payload[i];  // переводим посимвольный вывод из MQTT в строку для последующей печати в LED
  }

  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.println("-----------------------");
  #endif

  // вывод сообщения в LED
  m.shiftLeft(false,false);
  ledString[length] = '\0';
  
  // вывод на LED
  printStringWithShift(ledSpace, 100);    // вывод пробелов перед сообщением
  printStringWithShift(ledString, 100);   // вывод сообщения
  printStringWithShift(ledSpace, 100);    // вывод пробелов после сообщения
  // delay(3000); // для теста, что бы успеть увидеть конец сообщения и мусор, если он там есть

  // снова выводим время; @можно заморочится и вывести так же бегущей строкой, но лень
  m.clear();
  printHours();
  printMinutes();
  #if !defined(DOT_BLINK)
    m.writeSprite(15, 0, colon1); // двоеточие на LED
  #endif
}

// функция переподключения к WiFi и к mqtt
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    #ifdef SERIAL_DEBUG
      Serial.print("Attempting MQTT connection...");
    #endif
    // Create a random client ID
    // Этот кусок я пролюбил в основном проекте, надо или самому назначать уникальные имена клиентов или оставить этот кусок кода и он будет генерить рандом
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    //if (client.connect("ESP8266Client")) {
    // @ проверить работу авторизации mqtt
    if ( client.connect( clientId.c_str(), mqtt_user, mqtt_pass ) ) {
      #ifdef SERIAL_DEBUG
        Serial.println("connected");
      #endif
      // Once connected, publish an announcement...
      #ifdef TEST_PUB
        client.publish("test/outTopic", "hello world");
      #endif
      // ... and resubscribe
      client.subscribe("test/inTopic");   // подписка/переподписка на топик, где ожидаем входящие сообщения
      /*
      //Подписка на 5 топиков возможна......
      //Подписка долее, чем на 5 топиков роняет соединение к MQTT брокеру.
      client.subscribe("36/Output/01");
      client.subscribe("36/Output/02");
      client.subscribe("36/Output/03");
      client.subscribe("36/Output/04");
      client.subscribe("36/Output/05");
      */
    } else {
      #ifdef SERIAL_DEBUG
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
      #endif
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//  Фунция отправки ntp-запроса на указанный ntp-сервер
void sendNTPpacket(IPAddress& address) {
  #ifdef SERIAL_DEBUG
    Serial.println("sending NTP packet...");
  #endif
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

// постим сообщение в MQTT
void sendMQTT(){
  if (now - lastMsg < 0 ) lastMsg = 0;  // при переполнении millis() сработает и сбросит lastMsg, который будет обчень большим. @ можно проверить только через 50 дней работы
  if ( (now - lastMsg > MQTT_TIMEOUT) || (firstBOOT) )  {   // аналог задержки в 20с, но более умный способ, т.к. loop продолжает крутится и если мы отслеживаем наличие изменений в mqtt или еще где, то это отличный способ
    lastMsg = now;

    #ifdef TEST_PUB
      #ifdef SERIAL_DEBUG
          if ( firstBOOT ) Serial.println("NTP first run");
      #endif
      ++value;
      snprintf (msg, 75, "hello world #%ld", value);
      #ifdef SERIAL_DEBUG
        Serial.print("Publish message: ");
        Serial.println(msg);
      #endif  
      client.publish("test/outTopic", msg);
    #endif
  }
}

// обновление времени в NTP
void updateNTP()  {
  if (now - lastNTP < 0 ) lastNTP = 0;  // при переполнении millis() сработает и сбросит lastNTP, который будет обчень большим. @ можно проверить только через 50 дней работы
  if ( (now - lastNTP > NTP_TIMEOUT) || (firstBOOT) ) {    // будем обращаться к NTP только раз в NTP_TIMEOUT милисекунд, либо сразу, если первый запуск
      lastNTP = now;
      //get a random server from the pool
      WiFi.hostByName(ntpServerName, timeServerIP); 
      #ifdef SERIAL_DEBUG
          if ( firstBOOT ) Serial.println("NTP first run");
      #endif

      for (counterNTP=0; counterNTP < NTP_TRY; counterNTP++) {   // делаем NTP_TRY попыток получить данные
          #ifdef SERIAL_DEBUG
            Serial.print("NTP connect try: ");
            Serial.println(counterNTP);
          #endif
          sendNTPpacket(timeServerIP); // send an NTP packet to a time server
          // wait to see if a reply is available
          // delay(5000);  // на рабочем WiFi на индусский сервер с задержкой в 1с работало так себе, с задержкой 5с лучше
          delay(1000);    // даже с рабочего WiFi очень даже, если ntp-сервер российский(или любой юлижайший)

          cb = udp.parsePacket();
      
          if ( !(cb) ) {                // проверяем получен ли NTP-пакет
            #ifdef SERIAL_DEBUG
              Serial.println("no packet yet");
            #endif
            // @ можно приделать функцию перебора NTP
          }
          else {
            #ifdef SERIAL_DEBUG
              Serial.print("packet received, length=");
              Serial.println(cb);
            #endif

            // We've received a packet, read the data from it
            udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

            //the timestamp starts at byte 40 of the received packet and is four bytes,
            // or two words, long. First, esxtract the two words:
            highWord = word(packetBuffer[40], packetBuffer[41]);
            lowWord = word(packetBuffer[42], packetBuffer[43]);

            // combine the four bytes (two words) into a long integer
            // this is NTP time (seconds since Jan 1 1900):
            secsSince1900 = highWord << 16 | lowWord;

            #ifdef SERIAL_DEBUG
              Serial.print("Seconds since Jan 1 1900 = " );
              Serial.println(secsSince1900);
              // now convert NTP time into everyday time:
              Serial.print("Unix time = ");
            #endif

            // subtract seventy years:
            epoch = secsSince1900 - seventyYears;
            
            #ifdef SERIAL_DEBUG
              Serial.println(epoch);  // print Unix time
            #endif

            // print the hour, minute and second:
            secondsNTP = epoch % 60;

            minutesNTP = ((epoch % 3600) / 60);
            minutesNTP = minutesNTP + MM; //Add UTC Time Zone
            
            hoursNTP = (epoch  % 86400L) / 3600;    
            if(minutesNTP > 59) {      
              hoursNTP = hoursNTP + HH + 1; //Add UTC Time Zone  
              minutesNTP = minutesNTP - 60;
            }
            else {
              hoursNTP = hoursNTP + HH;
            }

            // обновляем время из NTP в основной цикл
            hours = hoursNTP;
            minutes = minutesNTP;

            #ifdef SERIAL_DEBUG
              Serial.print("The UTC time is -   ");
              if (hoursNTP<10) Serial.print("0");    // добавляем в переди 0 в выводе часов
              Serial.print(hoursNTP);
              Serial.print(":");
              if (minutesNTP<10) Serial.print("0");  // добавляем в переди 0 в выводе минут
              Serial.print(minutesNTP);
              Serial.print(":");
              if (secondsNTP<10) Serial.print("0");  // добавляем в переди 0 в выводе секунд
              Serial.println(secondsNTP);
            #endif

            counterNTP = NTP_TRY;   // если получили время, то можно более не бегать в цикле for
          } // END else првоерки получения NTP-пакета и вычисление hoursNTP и minutesNTP
      } // END for
  } // END if NTP_TIMEOUT, условия по обновлению времени из NTP
}

// раз в 60 секунд обновляем время локально, без NTP, локально может врать ~10сек в обе стороны, т.к. исполняется не по таймеру и зависит от скорости исполнения функций поддержания сетевого подключения
void updateLocalTime()  {
  if (now - lastMIN < 0 ) lastMIN = 0;      // при переполнении millis() сработает и сбросит
  if (now - lastMIN > 59000) {              // раз в 60 секунд обновляем количество минут
      lastMIN = now;
      minutes++;

      if(minutes > 59 ) {
        minutes = 0;
        hours++;
      }

      if(hours > 23 ) hours=0;
  } // END if lastMIN
}

// обновляем время на LED 
void updateLEDtime() {
  if ( hours != hours_old ) {
    hours_old = hours;
    m.clear();
    printMinutes();
    printHours();
    m.writeSprite(15, 0, colon1); // двоеточие на LED
  }

  if ( minutes != minutes_old ) {
    minutes_old = minutes;
    m.clear();
    printMinutes();
    printHours();
    m.writeSprite(15, 0, colon1); // двоеточие на LED
  }
}

void dotsBlinking() {
      if (now - lastSEC < 0 ) lastSEC = 0;
      if (now - lastSEC > 1000) {
        lastSEC = now;
        if ( blink )  m.writeSprite(15, 0, colon1);// рисуем двоеточие на LED
        if ( !blink ) m.writeSprite(15, 0, colon1_errase); // гасим двоеточие на LED
        blink = !blink;
      }
}